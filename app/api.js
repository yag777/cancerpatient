const DEFAULT_ENDPOINT = 'http://ec2-18-221-155-242.us-east-2.compute.amazonaws.com/api'

export default class API {
  constructor(endpoint = DEFAULT_ENDPOINT) {
    this.endpoint = endpoint
  }

  getPatients() {
    return fetch(`${this.endpoint}/patients`).then(res => res.json())
  }

  addPatient(payload = {}) {
    return fetch(`${this.endpoint}/patients`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    }).then(res => res.json())
  }

  addResults(id, results = {}) {
    return fetch(`${this.endpoint}/patients/${id}`, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(results)
    })
  }
}