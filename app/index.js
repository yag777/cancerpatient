/**
 * Application entry point
 */

// Load application styles
import 'styles/index.scss'


// Load external dependencies
import 'bootstrap'

// Load local files
import './controller'
