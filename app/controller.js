import Api from './api'
import AddResultsModal from './add-results'
import AddPatientModal from './add-patient'

var addResultsModal = new AddResultsModal()
var addPatientModal = new AddPatientModal()

var api = new Api()
var PatientList
var selectedPatient

addPatientModal.onSubmit(function(payload) {
  api.addPatient(payload)
    .then(getPatients)
    .catch((err) => {
      console.error(err)
      alert('Something went wrong :(')
    })
})


addResultsModal.onSubmit(function(payload) {
  api.addResults(selectedPatient.id, payload)
    .then(getPatients)
    .then(selectPatient.bind(null, selectedPatient.id))
    .catch((err) => {
      console.error(err)
      alert('Something went wrong :(')
    })
})

function getPatients() {
  return api.getPatients().then(function(serverlist) {
    var newPatient='<a href="#" class="list-group-item list-group-item-action active text-center" data-toggle="modal" data-target="#add-patient">Add Patient</a>'
    PatientList = serverlist
  
    for (var index = 0; index < PatientList.length; ++index) {
      newPatient+=`<li class="list-group-item list-group-item-action patient" id="${PatientList[index].id}">${PatientList[index].name}</li>`
    }
      
    list.innerHTML=newPatient
      
    document.querySelectorAll('.patient').forEach(function(element){
      element.addEventListener("click",displayPatientDetails)
    })
  }).catch((err) => {
    console.error(err)
    alert('Something went wrong :(')
  })
}

var list =document.getElementById('patient-list')
var gender =document.getElementById('patient-gender')
var age =document.getElementById('patient-age')
var suggestedTreatment=document.getElementById('suggested-treatment')
var patientResult=document.getElementById('patient-results')
var patientName=document.getElementById('patient-name')

function displayPatientDetails(event) {
  var id = event.target.id
  selectPatient(id)
}

function selectPatient(id) {
  selectedPatient = PatientList.find(patient => patient.id === id)
  var suggestedTreatmentValues=''
  var patientResultsValues='<tr> <th class="bg-light" scope="col">#</th><th class="bg-light" scope="col">Date</th><th class="bg-light" scope="col">Description</th></tr>'

  patientName.innerHTML = selectedPatient.name
  gender.innerHTML = selectedPatient.gender
  age.innerHTML = selectedPatient.age

  suggestedTreatment.innerHTML= selectedPatient.treatment.reduce((acc, treatment) => 
    acc + `<li>${treatment}</li>`
    , suggestedTreatmentValues)

  patientResult.innerHTML = selectedPatient.results.reduce((acc, result, index) => 
    acc + `<tr>
        <th scope="row">${index + 1}</th>
        <td>${result.date}</td>
        <td>${result.description}</td>
      </tr>`
    , patientResultsValues)
}

getPatients()