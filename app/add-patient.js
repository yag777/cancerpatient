export default class AddPatientModal {
  constructor() {
    this.nameInput = document.getElementById('nameInput')
    this.ageInput = document.getElementById('ageInput')
    this.genderInput = document.getElementById("genderInput")

    document.getElementById('add-patient-btn').addEventListener('click', () => {
      this.addPatient()
    })
  }

  addPatient() {
    const payload = {
      name: this.nameInput.value,
      age: this.ageInput.value,
      gender: this.genderInput.options[this.genderInput.selectedIndex].value,
      results: [],
      treatment: []
    }

    this.handler(payload)
  }

  onSubmit(handler) {
    this.handler = handler
  }
}