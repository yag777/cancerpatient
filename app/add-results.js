export default class AddResultsModal {
  constructor() {
    this.descriptionInput = document.getElementById('description-input')
    this.testResultsInput = document.getElementById('test-results-input')
    document.getElementById('add-results-btn').addEventListener('click', () => {
      this.addResults()
    })
  }

  addResults() {
    const payload = {
      description: this.descriptionInput.value,
      results: this.testResultsInput.value,
      date: new Date().toDateString()
    }

    this.handler(payload)
    this.descriptionInput.value = ''
    this.testResultsInput.value = ''
  }

  onSubmit(handler) {
    this.handler = handler
  }
}